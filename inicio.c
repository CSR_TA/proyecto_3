#include <stdio.h>
#include <stdlib.h>
#include "nombres.h"
//Definimos las varibles enteras
int nem_pon=0, rank=0, lenguaje=0, matematicas=0, historia=0, ciencias=0, res_historia, res_ciencias, conforme, i=0;
//definimos variable flotante 
float ultimo;
//Es la ultima funcion pedida en el proyecto, sera llamada luego desde la funcion menu
void x_facultad(){
//Abre el archivos con las carreras y sus puntajes
  
  FILE *f;
  
  f=fopen("uvmodificado.txt","r");
  
  if( f==NULL )
    printf("Error al abrir el fichero\n");
    
  else
  {
    while( !feof(f) )
      printf("%c",getc(f));
  }
}
//Pide el puntaje optenido por prueba mas el nem y ranking y los guarda en variables
void simulador(){
  
  int escogido_2, escogido, salir_2=1;
//do es parte del while, y sirve para que si o si lo recorra porlomenos una vez
  do{
//Es para verificar que pruebas dio
  printf("Dio la prueba de Historia y Ciencias Sociales:\n");
  printf("Si=1  o  No=2\n");
  scanf("%i",&res_historia);
  printf("Dio la prueba de Ciencias:\n");
  printf("Si=1  o  No=2\n");
  scanf("%i",&res_ciencias);

  printf("Llena los campos con tus distintos Puntajes:\n");
  printf("____________________________________________\n");
  printf("Ingrese su puntaje NEM:\n");
  scanf("%i", &nem_pon);
  printf("Ingrese su puntaje Ranking:\n");
  scanf("%i", &rank);
  printf("Ingrse su puntaje de Lenguaje y Comunicación\n");
  scanf("%i", &lenguaje);
  printf("Ingrese su puntaje Matematicas:\n");
  scanf("%i", &matematicas);
//Verifica  que prueba dio, y si no dio ni ciencia y historia, envia un mensaje para
  if (res_historia == 1){
    printf("Ingrese su puntaje de Historia y Ciencias Sociales\n");
    scanf("%i", &historia);
  }
  if (res_ciencias == 1){
    printf("Ingrese su puntaje de Ciencias\n"); 
    scanf("%i", &ciencias);
  }else{
    printf("Usted no puede postular a la Uv\n");//Sujeto a edicion
    salir_2=2;
  }

  printf("Revise si estan correctos los puntajes optenidos:\n");
  printf("_________________________________________________\n");
  
  printf("Su puntaje NEM: %i\n", nem_pon);
  printf("Su puntaje Ranking: %i\n", rank);
  printf("Su puntaje Lenguaje: %i\n", lenguaje);
  printf("Su puntaje Matematicas: %i\n", matematicas);
  if (res_historia == 1){
    printf("Su puntaje Historia: %i\n", historia);
  }
  if (res_ciencias == 1){
    printf("Su puntaje Ciencias: %i\n", ciencias);
  }
  printf("_________________________________________________\n");

  printf("¿Los puntajes ingresados son correctos?\n");
  printf("Si = 1  o  No = 2\n");
  scanf("%i", &conforme);
//Si esta conforme con sus notas, y no dio ni historia ni ciencias se sale del programa
  if (salir_2 == 2){
      printf("No puede postular si no dio ninguna de las dos pruebas optativas\n");
      //El exit(0) cierra el programa, esta incluido en la libreria stdlib.h
        exit(0); 
  }
//Si esta confome con los puntajes ingresados se sale del bucle while, en caso contrario lo buelve a recorrer
  }while(conforme != 1);
  /*
  printf("____________________________________________\n");
  printf("NEM: %i\n", nem);
  printf("Ranking: %i\n", ranking);
  printf("Lenguaje: %i\n", leng);
  printf("Matematicas: %i\n", mate);
  printf("Ciencias: %i\n", cs);
  printf("Historia: %i\n", hist);
  printf("____________________________________________\n");
  printf("Ultimo ingresado: %i\n", ingreso_min);

  if (cs>= hist){
    escogido=cs;
    escogido_2=ciencias;
  }else{
    escogido=hist;
    escogido_2=historia;
  }
  ultimo=((nem/10)*nem_pon)+((ranking/10)*rank)+((leng/10)*lenguaje)+((mate/10)*matematicas)+((escogido/10)*escogido_2);

*/
}

void ponderacion(nombres est[]){
/*
  int carrera, tope=53;
    printf("Ingrese el codigo de la carrera:\n");
    scanf("%d",&carrera);
    printf("%d\n",carrera );
    while((carrera == est[i].codigo_c) && (i<tope)) {
    	printf("%s,%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%d,%d",est[i].nombre_facultad,est[i].nombre_carrera,est[i].codigo_c,est[i].nem,est[i].ranking,est[i].leng,est[i].mate,est[i].hist,est[i].cs,est[i].pond_min,est[i].psu_min,
				est[i].ingreso_max, est[i].ingreso_min, est[i].cupo_psu, est[i].cupo_bea);
    	i++;
    }
    cargarnombres("uvmodificado.txt", est);
*/
  }

//Menu para ingresar a las diferentes categorias
int menu(nombres est[]){
  int opcion;
  do{
    printf("1)Ver ponderacion de cada carrera:\n");
    printf("2)Simulador de postulacion:\n");
    printf("3)Ponderacion por facultad:\n");
    printf("0)Salir:\n");
    scanf("%i",  &opcion);
//Swuitch para ingresar a cada categoria
    switch(opcion){
    case 1: ponderacion(est);
            break;
    case 2: simulador();
            break;
    case 3: x_facultad();
            break;
    }
  }while(opcion != 0);
}
int main(){
	nombres est[55];
  
  menu(est);
  return 0;
}